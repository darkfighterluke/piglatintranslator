package com.darkfighterluke.piglatintranslator;

import java.util.ArrayList;

public class Translator {
	public static final String NIL="nil";
	
	private String phrase;
	

	public Translator(String inputPhrase) {
		phrase=inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}
	
	public String translate() throws PigLatinException {
		if(phrase=="") {
			return NIL;
		}else {
			String translatedPhrase="";
			String[] words=phrase.split(" ");
			int i=0;
			for(String word : words) {
				if(word.contains("-")) {
					String[] compositeWordPieces=word.split("-");
					ArrayList<String> translatedPieces=new ArrayList<String>();
					for(String compositeWordPiece : compositeWordPieces) {
						translatedPieces.add(this.translateWord(compositeWordPiece));
					}
					if(i==0) {
						translatedPhrase=translatedPieces.get(0);
						for(int j=1; j<translatedPieces.size(); j++) {
							translatedPhrase+='-'+translatedPieces.get(j);
						}
					}else {
						translatedPhrase=' '+translatedPieces.get(0);
						for(String translatedPiece : translatedPieces) {
							translatedPhrase+='-'+translatedPiece;
						}
					}
				}else {
					if(i==0) {
						translatedPhrase=translatedPhrase+this.translateWord(word);
					}else {
						translatedPhrase=translatedPhrase+' '+this.translateWord(word);
					}
				}
				i++;
				
			}
			return translatedPhrase;		
		}
	}
	
	private String translateWord(String word) throws CaseNotAllowedException {
		boolean punctuationFirst=false;
		char punctuationFirstChar='_';
		boolean punctuationLast=false;
		char punctuationLastChar='_';
		boolean isUpperCase=false;
		boolean isTitleCase=false;
		
		if(this.isAnAllowedPunctuationChar(word.charAt(0))) {
			punctuationFirst=true;
			punctuationFirstChar=word.charAt(0);
			word=word.substring(1);
		}
		if(this.isAnAllowedPunctuationChar(word.charAt(word.length()-1))) {
			punctuationLast=true;
			punctuationLastChar=word.charAt(word.length()-1);
			word=word.substring(0, word.length()-1);
		}
		
		if(this.isWordUpperCase(word)) {
			isUpperCase=true;
		}else if(this.isWordTitleCase(word)) {
			isTitleCase=true;
		}else if(this.isWordCaseNotAllowed(word)) {
			throw new CaseNotAllowedException();
		}
		
		if(this.phraseStartsWithVowel(word)) {
			if(word.endsWith("y")) {
				word=word+"nay";
			}else if(this.phraseEndsWithVowel(word)) {
				word=word+"yay";
			}else {
				word=word+"ay";
			}
		}else if(this.phraseStartsWithConsonants(word)) {
			word=this.moveConsonants(word);
			word=word+"ay";
		}
		
		if(isUpperCase) {
			word=word.toUpperCase();
		}else if(isTitleCase) {
			word=Character.toUpperCase(word.charAt(0))+word.substring(1).toLowerCase();
		}
		
		if(punctuationFirst) {
			word=punctuationFirstChar+word;
		}
		if(punctuationLast) {
			word=word+punctuationLastChar;
		}
		
		return word;
	}
	
	private boolean isWordCaseNotAllowed(String word) {
		boolean hasUpperCaseChar=false;
		boolean hasLowerCaseChar=false;
		char[] charArray = word.toCharArray();
        for(int i=1; i < charArray.length; i++){
            if(Character.isUpperCase(charArray[i])) {
            	hasUpperCaseChar=true;
            }else {
            	hasLowerCaseChar=true;
            }
            
            if(hasUpperCaseChar && hasLowerCaseChar) {
            	return true;
            }
        }
        
        return false;
	}

	private boolean isWordTitleCase(String word) {
		if(Character.isUpperCase(word.charAt(0))){
			return !isWordUpperCase(word.substring(1));
		}
		return false;
	}

	private boolean isWordUpperCase(String word){
        char[] charArray = word.toCharArray();
        for(int i=0; i < charArray.length; i++){
            if(!Character.isUpperCase(charArray[i]))
                return false;
        }
        
        return true;
    }
	
	private boolean isAnAllowedPunctuationChar(char c) {
		return c=='!' ||c=='.' || c==',' || c==';' || c==':' || c=='?' || c=='\'' || c=='(' || c==')';
	}
	
	private String moveConsonants(String word) {
		for(int i=0; i<word.length(); i++) {
			if(!this.letterIsAVowel(word.charAt(0))) {
				word=word.substring(1)+word.charAt(0);
			}
		}
		return word;
	}
	
	private boolean phraseStartsWithConsonants(String word) {
		return !this.letterIsAVowel(word.charAt(0));
	}

	private boolean phraseStartsWithVowel(String word) {
		return letterIsAVowel(word.charAt(0));
	}
	
	private boolean phraseEndsWithVowel(String word) {
		return letterIsAVowel(word.charAt(word.length()-1));
	}
	
	private boolean letterIsAVowel(char letter) {
		letter=Character.toLowerCase(letter);
		return letter=='a' || letter=='e' || letter=='i' || letter=='o' || letter=='u';
	}

}
