package com.darkfighterluke.piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase="hello world";
		Translator translator=new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testEmptyInputPhrase() throws Exception {
		String inputPhrase="";
		Translator translator=new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testInputPhraseStartingWithVowelAndEndingWithY() throws Exception {
		String inputPhrase="any";
		Translator translator=new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testInputPhraseStartingWithUAndEndingWithY() throws Exception {
		String inputPhrase="utility";
		Translator translator=new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testInputPhraseStartingWithVowelAndEndingWithVowel() throws Exception {
		String inputPhrase="apple";
		Translator translator=new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testInputPhraseStartingWithVowelAndEndingWithConsonant() throws Exception {
		String inputPhrase="ask";
		Translator translator=new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testInputPhraseStartingWithASingleConsonant() throws Exception {
		String inputPhrase="hello";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testInputPhraseStartingWithMoreConsonants() throws Exception {
		String inputPhrase="known";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingMoreWords() throws Exception {
		String inputPhrase="hello world";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingCompositeWords() throws Exception {
		String inputPhrase="well-being";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingCompositeWordsAndNon() throws Exception {
		String inputPhrase="well-being hello";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellway-eingbay ellohay", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingExclamationMark() throws Exception {
		String inputPhrase="hello world!";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingPoint() throws Exception {
		String inputPhrase="hello world.";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay orldway.", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingComma() throws Exception {
		String inputPhrase="hello world,";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay orldway,", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingSemicolon() throws Exception {
		String inputPhrase="hello; world";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay; orldway", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingColon() throws Exception {
		String inputPhrase="hello :world";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay :orldway", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingQuestionMark() throws Exception {
		String inputPhrase="?hello world";
		Translator translator=new Translator(inputPhrase);
		assertEquals("?ellohay orldway", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingApostrophe() throws Exception {
		String inputPhrase="hello 'world";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay 'orldway", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingRoundParenthesis() throws Exception {
		String inputPhrase="hello (world)";
		Translator translator=new Translator(inputPhrase);
		assertEquals("ellohay (orldway)", translator.translate());
	}
	
	@Test
	public void testInputPhraseContainingDifferentPunctuations() throws Exception {
		String inputPhrase="!hello? (world)";
		Translator translator=new Translator(inputPhrase);
		assertEquals("!ellohay? (orldway)", translator.translate());
	}
	
	@Test
	public void testInputPhraseIsUpperCase() throws Exception {
		String inputPhrase="APPLE";
		Translator translator=new Translator(inputPhrase);
		assertEquals("APPLEYAY", translator.translate());
	}
	
	@Test
	public void testInputPhraseIsTitleCase() throws Exception {
		String inputPhrase="Hello";
		Translator translator=new Translator(inputPhrase);
		assertEquals("Ellohay", translator.translate());
	}
	
	@Test
	public void testInputPhraseIsTitleCaseWithPunctuations() throws Exception {
		String inputPhrase="!Hello!";
		Translator translator=new Translator(inputPhrase);
		assertEquals("!Ellohay!", translator.translate());
	}
	
	@Test
	public void testInputPhraseIsUpperCaseWithPunctuations() throws Exception {
		String inputPhrase=",APPLE'";
		Translator translator=new Translator(inputPhrase);
		assertEquals(",APPLEYAY'", translator.translate());
	}
	
	@Test(expected=PigLatinException.class)
	public void testInputPhraseCaseNotAllowed() throws Exception{
		String inputPhrase="biRd";
		Translator translator=new Translator(inputPhrase);
		translator.translate();
	}

}
